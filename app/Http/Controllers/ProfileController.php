<?php

namespace App\Http\Controllers;

use App\Profile;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$profile = Profile::with('user')->orderBy('id','asc')->get();
		return view('admin.profile.index', compact('profile'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		// dd($request->all());
		$request->validate([
			'nama_lengkap'  => 'required',
			'nik'           => 'required',
			'jenis_kelamin' => 'required',
			'tempat'        => 'required',
			'tanggal'       => 'required',
			'alamat'        => 'required',
			'agama'         => 'required',
			'foto_profil'   => 'mimes:jpeg,png,jpg,gif,svg|max:2048',
			'email'         => ['required', 'string', 'email', 'max:255', 'unique:users'],
			'password'      => ['required', 'string', 'min:8'],
		]);
		
		$user = new User;
		$user->name     = $request->nama_lengkap;
		$user->email    = $request->email;
		$user->password = Hash::make($request->password);
		$user->save();

		$profil = Profile::create($request->all());
		if (is_null($request->foto_profil)) {
			$imageName 	= 'img/default.png';
		} else {
			$nameimg 	= time().'.'.$request->foto_profil->extension();  
			$request->foto_profil->move(public_path('img'), $nameimg);
			$imageName 	= 'img/'.$nameimg;
		}
		$profil->foto_profil 	= $imageName;
		$profil->user_id 		= $user->id;
		$profil->save();

		
		return redirect()->back();
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Profile  $profile
	 * @return \Illuminate\Http\Response
	 */
	public function show(Profile $profile)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\Profile  $profile
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Profile $profile)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Profile  $profile
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Profile $profile)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Profile  $profile
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		$profil = Profile::find($id);
		$userid = User::where('id',$profil->user_id)->first();
		$userid->delete();
		$profil->delete();
		return redirect()->back();
	}
}
