<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;

class HomeController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}
	public function index()
	{
		$profile = Profile::where('user_id',auth()->user()->id)->first();
		return view('home', compact('profile'));
	}
	public function adminHome()
	{
		return view('admin.home');
	}
	public function createProfile(Request $request){
		$request->validate([
			'nama_lengkap'  => 'required',
			'nik'           => 'required',
			'jenis_kelamin' => 'required',
			'tempat'        => 'required',
			'tanggal'       => 'required',
			'alamat'        => 'required',
			'agama'         => 'required',
			'foto_profil'   => 'mimes:jpeg,png,jpg,gif,svg|max:2048'
		]);

		$profil = Profile::create($request->all());
		if (is_null($request->foto_profil)) {
			$imageName = 'img/default.png';
		} else {
			$nameimg 	= time().'.'.$request->foto_profil->extension();  
			$request->foto_profil->move(public_path('img'), $nameimg);
			$imageName 	= 'img/'.$nameimg;
		}
		$profil->foto_profil 	= $imageName;
		$profil->user_id 		= auth()->user()->id;
		$profil->save();

		
		return redirect()->back();
	}
	public function updateProfile(Request $request){
		$profil 				= Profile::where('user_id',auth()->user()->id)->first();
		$profil->nama_lengkap 	= $request->nama_lengkap;
		$profil->nik 			= $request->nik;
		$profil->jenis_kelamin 	= $request->jenis_kelamin;
		$profil->tempat 		= $request->tempat;
		$profil->tanggal 		= $request->tanggal;
		$profil->alamat 		= $request->alamat;
		$profil->agama 			= $request->agama;

		if (is_null($request->foto_profil)) {
			$imageName 	= $profil->foto_profil;
		} else {
			$nameimg 	= time().'.'.$request->foto_profil->extension();  
			$request->foto_profil->move(public_path('img'), $nameimg);
			$imageName 	= 'img/'.$nameimg;
		}

		$profil->foto_profil 	= $imageName;
		// dd($profil);
		$profil->save();
		return redirect()->back();
	}
}
