<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table    = 'profiles';
    protected $fillable = ['user_id','nama_lengkap','nik','jenis_kelamin','tempat','tanggal','alamat','agama','foto_profil'];
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
