@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">Edit User Profile</div>
				<div class="card-body">
					@if(is_null($profile))
					<form action="{{ route('create.profile') }}" enctype="multipart/form-data" method="POST">
						@csrf
						@include('admin.profile.form')
					</form>
					@else
					<form action="{{ route('update.profile') }}" enctype="multipart/form-data" method="POST">
						@csrf
						@method('PATCH')
						@include('admin.profile.form_edit')
					</form>
					@endif
				</div>
			</div>
		</div>
	</div>
</div>
@endsection