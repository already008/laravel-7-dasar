<div class="row">
	<div class="col-md-6">
		<div class="form-group">
			<label for="">Nama Lengkap</label>
			<input type="text" class="form-control" id="nama_lengkap" name="nama_lengkap" placeholder="Nama Lengkap">
		</div>
		<div class="form-group">
			<label for="">Email</label>
			<input type="text" class="form-control" id="email" name="email" placeholder="Email Anda">
		</div>
		<div class="form-group">
			<label for="">Password</label>
			<input type="password" class="form-control" id="password" name="password" placeholder="Password Anda">
		</div>
		<div class="form-group">
			<label for="">Alamat</label>
			<textarea name="alamat" id="alamat" rows="3" class="form-control" placeholder="Alamat Anda"></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label for="">NIK</label>
			<input type="number" class="form-control" id="nik" name="nik" placeholder="NIK">
		</div>
		<div class="form-group">
			<label for="">Jenis Kelamin</label>
			<select name="jenis_kelamin" id="jenis_kelamin" class="form-control">
				<option>Pilih Jenis Kelamin</option>
				<option value="laki-laki">Laki-Laki</option>
				<option value="perempuan">Perempuan</option>
			</select>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col">
					<label for="">Tempat Lahir</label>
					<input type="text" class="form-control" name="tempat" id="tempat" placeholder="Tempat Lahir">
				</div>
				<div class="col">
					<label for="">Tanggal Lahir</label>
					<input type="text" class="form-control" name="tanggal" id="tanggal" placeholder="Tanggal Lahir">
				</div>
			</div>
		</div>
		<div class="form-group">
			<label for="">Agama</label>
			<select name="agama" id="agama" class="form-control">
				<option>Pilih Agama</option>
				<option value="islam">Islam</option>
				<option value="hindu">Hindu</option>
				<option value="buddha">Buddha</option>
				<option value="kristen">Kristen</option>
			</select>
		</div>
		<div class="form-group">
			<label for="">Foto Profil</label>
			<input type="file" name="foto_profil" class="form-control" accept="image/png, image/jpg, image/jpeg">
		</div>
	</div>
	<div class="col-md-12">
		<div class="form-group text-center">
			<button type="submit" class="btn btn-primary">Submit Data</button>
		</div>
	</div>
</div>
