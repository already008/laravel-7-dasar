<table id="example" class="table table-striped table-bordered" style="width:100%">
	<thead>
		<tr>
			<th>No.</th>
			<th>Email Login</th>
			<th>Nama Lengkap</th>
			<th>Jenis Kelamin</th>
			<th>TTL</th>
			<th>Agama</th>
			<th>Foto Profil</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		@foreach($profile as $p)
		<tr>
			<td>{{ $loop->iteration }}</td>
			<td>{{ $p->user->email }}</td>
			<td>{{ ucwords($p->nama_lengkap) }}</td>
			<td>{{ ucwords($p->jenis_kelamin) }}</td>
			<td>{{ ucwords($p->tempat) }}, {{ $p->tanggal }}</td>
			<td>{{ ucwords($p->agama) }}</td>
			<td><img src="{{ asset($p->foto_profil) }}" class="rounded-circle" alt="{{ ucwords($p->nama_lengkap) }}" style="width: 30%;"></td>
			<td>
				<form action="{{ route('admin.user-profile.destroy',$p->id) }}" method="POST">
					@csrf
					@method('DELETE')
					<button type="submit" class="btn btn-danger" onclick="return confirm('yakin mau hapus data ini?');">Delete</button>
				</form>
			</td>
		</tr>
		@endforeach
	</tbody>
	<tfoot>
		<tr>
			<th>No.</th>
			<th>Email Login</th>
			<th>Nama Lengkap</th>
			<th>Jenis Kelamin</th>
			<th>TTL</th>
			<th>Agama</th>
			<th>Foto Profil</th>
			<th></th>
		</tr>
	</tfoot>
</table>