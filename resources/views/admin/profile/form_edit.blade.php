<div class="row">
	<div class="col-md-6">
		<div class="form-group">
			<label for="">Nama Lengkap</label>
			<input type="text" class="form-control" id="nama_lengkap" name="nama_lengkap" placeholder="Nama Lengkap" value="{{ $profile->nama_lengkap }}">
		</div>
		<div class="form-group">
			<label for="">Alamat</label>
			<textarea name="alamat" id="alamat" rows="3" class="form-control" placeholder="Alamat Anda">{{ $profile->alamat }}</textarea>
		</div>
		<div class="form-group">
			<label for="">NIK</label>
			<input type="number" class="form-control" id="nik" name="nik" placeholder="NIK" value="{{ $profile->nik }}">
		</div>
		<div class="form-group">
			<label for="">Jenis Kelamin</label>
			<select name="jenis_kelamin" id="jenis_kelamin" class="form-control">
				<option>Pilih Jenis Kelamin</option>
				<option value="laki-laki" {{ $profile->jenis_kelamin == 'laki-laki' ? 'selected' : '' }}>Laki-Laki</option>
				<option value="perempuan" {{ $profile->jenis_kelamin == 'perempuan' ? 'selected' : '' }}>Perempuan</option>
			</select>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<div class="row">
				<div class="col">
					<label for="">Tempat Lahir</label>
					<input type="text" class="form-control" name="tempat" id="tempat" placeholder="Tempat Lahir" value="{{ $profile->tempat }}">
				</div>
				<div class="col">
					<label for="">Tanggal Lahir</label>
					<input type="text" class="form-control" name="tanggal" id="tanggal" placeholder="Tanggal Lahir" value="{{ $profile->tanggal }}">
				</div>
			</div>
		</div>
		<div class="form-group">
			<label for="">Agama</label>
			<select name="agama" id="agama" class="form-control">
				<option>Pilih Agama</option>
				<option value="islam" {{ $profile->agama == 'islam' ? 'selected' : '' }}>Islam</option>
				<option value="hindu" {{ $profile->agama == 'hindu' ? 'selected' : '' }}>Hindu</option>
				<option value="buddha" {{ $profile->agama == 'buddha' ? 'selected' : '' }}>Buddha</option>
				<option value="kristen" {{ $profile->agama == 'kristen' ? 'selected' : '' }}>Kristen</option>
			</select>
		</div>
		<div class="form-group">
			<label for="">Foto Profil</label>
			<input type="file" name="foto_profil" class="form-control" accept="image/png, image/jpg, image/jpeg">
			<img src="{{ asset($profile->foto_profil) }}" class="img-fluid" width="30%">
		</div>
	</div>
	<div class="col-md-12">
		<div class="form-group text-center">
			<button type="submit" class="btn btn-primary">Edit Data</button>
		</div>
	</div>
</div>
