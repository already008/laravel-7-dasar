@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-12 mb-3">
			<div class="card">
				<div class="card-header">
					<span class="badge badge-success">Add User Profile</span>
				</div>
				<div class="card-body">
					<div class="col-md-12">
						<form action="{{ route('admin.user-profile.store') }}" enctype="multipart/form-data" method="POST">
							@csrf
							@include('admin.profile.form')
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<span class="badge badge-primary">View User Profile</span>
				</div>
				<div class="card-body">
					@include('admin.profile.table')
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@push('style')
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap4.min.css">
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@ttskch/select2-bootstrap4-theme/dist/select2-bootstrap4.min.css">
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
@endpush
@push('script')
<script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<script>
	$(document).ready(function() {
	    $('#example').DataTable();
	    $('select').select2({
		    theme: 'bootstrap4',
		});
		$('#tanggal').datepicker({
            uiLibrary: 'bootstrap4'
        });
	} );
</script>
@endpush