<?php

use Illuminate\Database\Seeder;
use App\User;

class CreateUsersSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$user = [
			[
				'name'=>'Admin',
				'email'=>'developer@argiacyber.com',
				'is_admin'=>'1',
				'password'=> bcrypt('$y*NUp7nEyXj'),
			],
			[
				'name'=>'User',
				'email'=>'user@argiacyber.com',
				'is_admin'=>'0',
				'password'=> bcrypt('123456'),
			],
		];

		foreach ($user as $key => $value) {
			User::create($value);
		}
	}
}
