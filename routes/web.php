<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/create-profile','HomeController@createProfile')->name('create.profile');
Route::patch('/update-profile','HomeController@updateProfile')->name('update.profile');

Route::middleware(['is_admin'])->name('admin.')->prefix('admin')->group(function(){
	Route::get('home', 'HomeController@adminHome')->name('home');
	Route::resource('user-profile', 'ProfileController');
});